from units import convert


def test1():
    assert convert(1, 'km', 'm') == 1000


def test2():
    assert convert(100, 'sm', 'm') == 1


def test3():
    assert convert(1, 'km', 'dm') == 10000


def test4():
    assert convert(1, 'sm', 'mm') == 10
